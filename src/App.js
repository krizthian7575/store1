import React, { Component } from 'react';
//import logo from './logo.svg';
import { Navbar, NavbarBrand } from 'reactstrap';
import Menu from './components/MenuComponent';
//import './App.css';
import {PRENDAS} from './shared/prendas';

class App extends Component{   
  constructor(props){
    super(props);
    this.state={
      platos: PRENDAS
    };
  }
  render() {
    return (
      <div className="App">
        <Navbar dark color="primary">
          <div className='container'> 
            <NavbarBrand href='/'>STORE - CHICHU</NavbarBrand>
          </div>
        </Navbar>
        <Menu platos={this.state.platos}/>
      </div>
    );
  }
}
export default App;
