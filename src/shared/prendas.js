export const PRENDAS =
    [
        {
        id: 0,
        name: 'Pantalón',
        image: 'assets/images/pantalon1.jpg',
        category: 'ropa masculina',
        label: 'Frío',
        price: '48.50',
        description: 'Pantalón talla M color Negro',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "genial",
            author: "Juan Pérez",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 4,
            comment: "necesito otro color",
            author: "Paul Jara",
            date: "2014-09-05T17:57:28.556094Z"
            }
         ]                        
        },
        {
        id: 1,
        name:'Camiseta',
        image: 'assets/images/camiseta1.jpg',
        category: 'ropa masculina',
        label:'Caliente',
        price:'34.00',
        description:'Camiseta talla S color Azul',
        comments: [
            {
            id: 0,
            rating: 5,
            comment: "La mejor",
            author: "Carla A",
            date: "2012-10-16T17:57:28.556094Z"
            },
            {
            id: 1,
            rating: 4,
            comment: "Buenisima!!",
            author: "Jaime Pineda",
            date: "2014-09-05T17:57:28.556094Z"
            },
            {
            id: 2,
            rating: 3,
            comment: "Falta variedad de colores",
            author: "Miguel Acosta",
            date: "2015-02-13T17:57:28.556094Z"
            }
        ]
        }
    ];